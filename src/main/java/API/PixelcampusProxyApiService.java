package API;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import io.javalin.Javalin;
import io.javalin.http.UnauthorizedResponse;
import org.fsi.pixelcampusproxy.PixelcampusProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

public class PixelcampusProxyApiService {

    private PixelcampusProxyApiService() {
    }

    private static Javalin app;

    private static final Logger logger = LoggerFactory.getLogger(PixelcampusProxy.class);

    public static void init(String AUTH_KEY){
        app = Javalin.create();

        //checks if the api key is correct. If not, dont register handlers!
        app.beforeMatched(ctx -> {
            String authHeader = ctx.header("key");

            if (authHeader == null || !authHeader.equals(AUTH_KEY)) {
                ctx.status(401).result("Unauthorized");

                logger.warn(ctx.host() + " tried to access " + ctx.endpointHandlerPath() + " with invalid AUTH key.");

                throw new UnauthorizedResponse();
            }
        });

        app.get("/players", ctx -> ctx.json(getOnlinePlayers()));
        app.get("/servers", ctx -> ctx.json(getNetworkServers()));
        app.start(7070);
    }

    private static String getOnlinePlayers(){

        JsonArray onlinePlayers = new JsonArray();
        JsonObject player;

        for (Player p : PixelcampusProxy.getProxy().getAllPlayers()) {


            player = new JsonObject();
            player.addProperty("name",p.getUsername());
            player.addProperty("uuid",p.getUniqueId().toString());
            player.addProperty("server",p.getCurrentServer().get().getServer().getServerInfo().getName());
            player.addProperty("ping",p.getPing());
            player.addProperty("client",p.getClientBrand());
            player.addProperty("ip",p.getRemoteAddress().toString());

            onlinePlayers.add(player);
        }

        return onlinePlayers.toString();
    }

    private static String getNetworkServers(){

        JsonArray networkServers = new JsonArray();
        JsonObject server;

        for (RegisteredServer s : PixelcampusProxy.getProxy().getAllServers()) {


            server = new JsonObject();
            server.addProperty("name",s.getServerInfo().getName());
            server.addProperty("online",s.getPlayersConnected().size());
            try {
                server.addProperty("max",s.ping().get().getPlayers().get().getMax());
                server.addProperty("version", s.ping().get().getVersion().getName());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }

            networkServers.add(server);
        }

        return networkServers.toString();
    }
}
