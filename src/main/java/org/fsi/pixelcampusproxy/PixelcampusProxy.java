package org.fsi.pixelcampusproxy;

import API.PixelcampusProxyApiService;
import HubCommand.HubCommand;
import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandManager;
import com.velocitypowered.api.command.CommandMeta;
import com.velocitypowered.api.command.SimpleCommand;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import org.slf4j.Logger;

@Plugin(
        id = "pixelcampusproxy",
        name = "PixelcampusProxy",
        version = BuildConstants.VERSION
)
public class PixelcampusProxy {

    private static ProxyServer proxy;

    @Inject
    public PixelcampusProxy(ProxyServer proxy) {
        this.proxy = proxy;
    }

    public static ProxyServer getProxy(){
        return proxy;
    }

    @Inject
    private Logger logger;


    private static String AUTH_KEY = "=dZq%!i,V05-}QS{}{~wQ5<=+h9:_>8)G$t&W[9(";

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {

        //API to fetch server and Player Information
        PixelcampusProxyApiService.init(AUTH_KEY);

        //hub command
        CommandManager commandManager = proxy.getCommandManager();
        CommandMeta commandMeta = commandManager.metaBuilder("hub")
                .aliases("lobby", "l")
                .plugin(this)
                .build();

        SimpleCommand hubCommand = new HubCommand();
        commandManager.register(commandMeta, hubCommand);
    }
}
