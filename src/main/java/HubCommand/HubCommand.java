package HubCommand;

import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.command.SimpleCommand;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ServerConnection;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.fsi.pixelcampusproxy.PixelcampusProxy;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class HubCommand implements SimpleCommand {

    private final String lobbyName = "lobby"; //todo: config dafür erstellen

    @Override
    public void execute(Invocation invocation) {
        CommandSource source = invocation.source();

        if(source instanceof Player p){

            ServerConnection currentServer = p.getCurrentServer().orElse(null);


            if(currentServer != null && currentServer.getServerInfo().getName().equals(lobbyName)){
                p.sendMessage(Component.text("You are already in " + lobbyName).color(NamedTextColor.RED));
                return;
            }

            try {
                p.createConnectionRequest(
                        PixelcampusProxy.getProxy().getServer(lobbyName).orElse(null)
                ).connect();

            }catch (Exception e){
                p.sendMessage(Component.text("Could not connect to Lobby").color(NamedTextColor.RED));
            }
        }
    }

    @Override
    public List<String> suggest(Invocation invocation) {
        return SimpleCommand.super.suggest(invocation);
    }

    @Override
    public CompletableFuture<List<String>> suggestAsync(Invocation invocation) {
        return SimpleCommand.super.suggestAsync(invocation);
    }

    @Override
    public boolean hasPermission(Invocation invocation) {
        return SimpleCommand.super.hasPermission(invocation);
    }
}
